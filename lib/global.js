const EventEmitter = require('events');

const defaultConfig = require('../config/default');
const userConfig = require(`../config/user.${process.env.USER || 'root'}`);

class Renderer extends EventEmitter {
    constructor() {
        super();

        this.config = Object.assign(defaultConfig, userConfig);
    }
}

global.Renderer = new Renderer();

process.on('unhandledRejection', error => console.log('unhandledRejection', error));