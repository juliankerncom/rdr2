const ADAPTERS = [
    'http'
];

ADAPTERS.forEach(a => {
    const Adapter = require(`./${a}`);

    const instance = new Adapter();
    instance.start()

    module.exports[a] = instance;
})