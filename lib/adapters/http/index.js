const http = require('http');

const PORT = Renderer.config.port

module.exports = class HTTPAdapter {
    constructor() {
        this.server = http.createServer((...data) => this.requestHandler.call(this, ...data))
    }

    start() {
        this.server.listen(PORT, (err) => {
            if (err) return console.error(err);

            console.log(`HTTP server running on port ${PORT}`);
        });
    }

    requestHandler(req, res) {
        this.doRouting(req, res);
        // console.log('req.url', req.url);
    }

    async doRouting(req, res) {
        switch(req.url) {
            case '/renderengine':
                try {
                    if (req.method !== 'POST') throw 'no POST';

                    const postData = await getPOSTData(req);
                    Renderer.emit('receivedRequest', JSON.parse(postData), (data) => res.end(data));
                } catch(e) {
                    console.error(e);
                    res.end('error');
                }

                break;

            default:
                res.end('done');
        }
    }
}

function getPOSTData(req) {
    return new Promise((resolve, reject) => {
        let body = '';
        req.on('data', chunk => {
            body += chunk.toString(); // convert Buffer to string
        });
        req.on('end', () => {
            resolve(body);
        });
    })
}