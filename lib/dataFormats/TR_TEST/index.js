module.exports = class TRTestDataFormat {
    constructor() {

    }

    test(body) {
        console.log('TRTEST.test', body)
        return body.testScenario1;
    }

    parse(body, callback) {
        Object.entries(body).forEach(([_, data]) => {
            let context = Object.assign({}, data);

            delete context.template;
            delete context.data;

            // console.log('TRDataFormat parse data', data);

            context.devEnvironment = Renderer.config.devEnvironment;

            Renderer.emit('doRender', {
                context,
                template: data.template,
                data: data.data
            }, (body) => this.send(body, callback));
        })
    }

    send(body, callback) {
        callback(JSON.stringify({
            _0: {
                html: body,
                error: null,
                status: 200,
                meta: {}
            }
        }));
    }
}