module.exports = class TRDataFormat {
    constructor() {

    }

    test(body) {
        return body.requestId && body.scenarios;
    }

    parse(body, callback) {
        Object.entries(body.scenarios).forEach(([_, data]) => {
            let context = Object.assign({}, data);

            delete context.template;
            delete context.data;

            // console.log('TRDataFormat parse data', data);

            context.devEnvironment = Renderer.config.devEnvironment;
            context.country = context.locale.split('_')[1].split('.')[0];

            Renderer.emit('doRender', {
                context,
                template: data.template,
                data: data.data
            }, (body) => this.send(body, callback));
        })
    }

    send(body, callback) {
        callback(JSON.stringify({
            _0: {
                html: body,
                error: null,
                status: 200,
                meta: {}
            }
        }));
    }
}