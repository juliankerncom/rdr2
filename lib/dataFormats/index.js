let FORMATS = [
    'TR'
];

let parsers = [];


FORMATS.map(e => {
    const Format = require(`./${e}`);

    parsers.push(new Format());
    // instance.start();
})

Renderer.on('receivedRequest', (body, callback) => {
    let format = {};


    const found = parsers.some(e => {
        format = e;
        return e.test(body);
    });

    if (found) {
        // console.log('format found!')
        return format.parse(body, callback);
    }


    console.error('none found');
});