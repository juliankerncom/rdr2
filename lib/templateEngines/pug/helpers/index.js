module.exports = (...params) => Object.assign(
    require('./url')(...params),
    require('./i18n')(...params),
    require('./featureSwitches')(...params),
    require('./currency')(...params),
);