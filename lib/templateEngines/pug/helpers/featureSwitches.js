const urlHelpers = require('../../shared/helpers/url');

module.exports = () => ({
    isFeatureEnabled: function (fs) {
        return this.context.TR.enabledFeatureSwitches.includes(fs)
    }
})