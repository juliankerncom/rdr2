const currencyFormatter = require('currency-formatter');

module.exports = () => ({
    currency: function (price, currency) {
        return currencyFormatter.format(price, { code: currency })
    }
})