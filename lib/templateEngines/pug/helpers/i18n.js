const i18nHelpers = require('../../shared/helpers/i18n');

module.exports = (i18nProvider) => ({
    mssg: function (messageKey) {
        return i18nHelpers.mssg(messageKey, this.context.TR.locale, i18nProvider);
    }
})