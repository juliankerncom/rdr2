const urlHelpers = require('../../shared/helpers/url');

module.exports = () => ({
    staticUrl: function (staticPath) {
        return urlHelpers.staticUrl(staticPath, this.context.TR.baseStatic)
    }
})