const fs = require('fs');

// var logger = require('../utils/logger');
const pug = require('pug');

let compiled = {};
let templatesList = {};
let helpers = {};

module.exports = class DustEngine {
    constructor(templates, i18nProvider) {
        templatesList = templates;
        helpers = require('./helpers')(i18nProvider);
        this.context = {
            languages: ['de', 'en'],
        };

        Object.keys(templatesList).forEach(p => cacheTemplate(p));
    }

    templateChanged(key, path) {
        cacheTemplate(key, path);
    }

    async render(template, context, data) {
        // let contextData = dust.context(Object.assign({}, this.context, context)).push(data);
        let contextData = Object.assign({}, { global: this.context, helpers: {} }, { TR: context }, data);

        Object.entries(helpers).forEach(([name, helper]) => {
            contextData.helpers[name] = helper.bind({ context: contextData })
        });

        // console.log('contextData', contextData);

        return compiled[template](contextData);
    }
}

function cacheTemplate(key, path) {
    let src = fs.readFileSync(templatesList[key].path || path) + '';

    // src = handleLPSnippets(src);

    compiled[key] = pug.compile(src);
}