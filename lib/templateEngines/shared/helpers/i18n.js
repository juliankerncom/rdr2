module.exports = {
    mssg: (messageKey, locale, i18nProvider) => {
        let packName = messageKey.split('.')[0];
        messageKey = messageKey.split('.').splice(1).join('.');

        return i18nProvider.getSnippet(packName, messageKey, locale.split('_')[0], locale.split('_')[1].split('.')[0]);
    }
}