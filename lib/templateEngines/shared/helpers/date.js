var moment = require('moment-timezone');

module.exports = {
    date: (locale, format) => {
        return moment().locale(locale.split('.')[0].split('_')[0]).format(format);
    }
}