const { URL } = require('url');

module.exports = {
    url: (path, base) => {
        return new URL(path, base).toString();
    },
}