const readline = require('readline');

const objectAssignDeep = require(`object-assign-deep`);
const glob = require('glob');
const chokidar = require('chokidar');


let languageCache = { languagePacks: {} };

const LANGUAGE_FILE_GLOB = `${Renderer.config.languageDir}/**/*.json`;

class i18nProvider {
    constructor() {

    }

    async init() {
        if (Renderer.config.preloadI18n) await this.cacheI18n();
        if (Renderer.config.watchI18n) this.watchI18n();
    }

    watchI18n() {
        chokidar.watch(LANGUAGE_FILE_GLOB, { ignoreInitial: true }).on('all', async (event, path) => {
            console.log(`Detected ${event} event for i18n, (re-)caching `, path);
            this.cacheI18nFile(path);
        });
    }

    cacheI18nFile(path) {
        // check whitelist again for watcher
        if(Renderer.config.i18nWhitelist && !new RegExp(Renderer.config.i18nWhitelist).test(path)) return;

        languageCache = objectAssignDeep(languageCache, require(path));
    }

    async cacheI18n() {
        return new Promise((resolve, reject) => {
            glob(LANGUAGE_FILE_GLOB, (err, matches) => {
                let index = 1;

                matches.forEach(path => {
                    readline.cursorTo(process.stdout, 0);
                    process.stdout.write(`Loading languagefiles: ${index}/${matches.length}`);

                    if(Renderer.config.i18nWhitelist && !new RegExp(Renderer.config.i18nWhitelist).test(path)) return index++;

                    this.cacheI18nFile(path);
                    index++;
                });

                console.log('...done.');

                resolve();
            })
        });
    }

    loadPack(packName, language) {
        languageCache = objectAssignDeep(languageCache, require(`${Renderer.config.languageDir}/${language}/${packName}.json`));
        console.log(`Loaded pack: ${packName} for language ${language}`);
    }

    getSnippet(packName, snippetKey, language, country, retry = false) {
        if (!snippetKey) return new Error;

        try {
            let result = languageCache.languagePacks[packName][language][snippetKey];
            if (!result && language != 'en') result = languageCache.languagePacks[packName]['en'][snippetKey];
            if (!result && language != 'de') result = languageCache.languagePacks[packName]['de'][snippetKey];

            return result || new Error;
        } catch (e) {
            if (retry || (languageCache.languagePacks[packName] && languageCache.languagePacks[packName][language])) return new Error;

            // assume the language && pack isn't loaded yet
            this.loadPack(packName, language);

            return this.getSnippet(packName, snippetKey, language, country, true);
        }
    }
}

module.exports = new i18nProvider();