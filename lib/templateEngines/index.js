const fs = require('fs');
const path = require('path');

const glob = require('glob');
const chokidar = require('chokidar');
// const htmlMinifier = require('html-minifier');

const Minimize = require('minimize')
    , minimize = new Minimize({
        quotes: true
    });

const i18nProvider = require('./i18nProvider');

let templateCache = {};

const ENGINES = [
    'dust',
    'pug',
    'hbs',
];
let TEMPLATE_FILE_GLOB = `${Renderer.config.templateDirs.length > 1 ? `{${Renderer.config.templateDirs.join(',')}}` : Renderer.config.templateDirs[0]}`;
TEMPLATE_FILE_GLOB += `/**/*.${ENGINES.length > 1 ? `{${ENGINES.join(',')}}`: ENGINES[0]}`;

let engines = {};

init();


async function init() {
    await cacheTemplates();
    if (Renderer.config.watchTemplates) watchTemplates();

    await i18nProvider.init();


    ENGINES.forEach(async e => {
        let Engine;

        try {
            Engine = require(`./${e}`);

            const instance = new Engine(
                Object.entries(templateCache)
                .filter(([_, t]) => t.type === e)
                .reduce((obj, [k, v]) => Object.assign(obj, { [k]: v }), {}),
                i18nProvider
            );

            engines[e] = instance;
            // instance.start();
        } catch(err) {
            console.error(err);
            console.log(`Engine ./${e} could not be loaded`);
        }
    })
}


Renderer.on('doRender', async (data, callback) => {
    data.template = path.normalize(data.template);

    if (!templateCache[data.template]) {
        throw `Template ${data.template} not found!`;
    }

    const startRender = process.hrtime();

    let rendered = await engines[templateCache[data.template].type].render(data.template, data.context, data.data);

    const doneRender = process.hrtime(startRender);
    const startMinify = process.hrtime();

    if (Renderer.config.minifyHTML) {
        // rendered = htmlMinifier.minify(rendered, {
        //     collapseWhitespace: true
        // });
        rendered = minimize.parse(rendered);
    }

    const doneMinify = process.hrtime(startMinify);

    console.log(`${data.template} rendered in ${formatHRTime(doneRender)}ms, minified in ${formatHRTime(doneMinify)}ms`);
    callback(rendered);
});

function watchTemplates() {
    chokidar.watch(TEMPLATE_FILE_GLOB, { ignoreInitial: true }).on('all', async (event, path) => {
        console.log(`Detected ${event} event for templates, (re-)caching `, path.replace(new RegExp(`(${Renderer.config.templateDirs.join('|')})/`), ''));
        cacheTemplate(path, event)
    });
}

function cacheTemplate(path, event) {
    let key = path.split('.');
    let type = key.pop();
    key = key.join('.');
    key = key.replace(new RegExp(`(${Renderer.config.templateDirs.join('|')})/`), '');

    if (event === 'unlink') delete templateCache[key];

    templateCache[key] = {
        type,
        path: path,
    };

    if (event === 'change' && engines[type].templateChanged) {
        engines[type].templateChanged(key, path);
    }
}

function cacheTemplates() {
    return new Promise((resolve, reject) => {
        glob(TEMPLATE_FILE_GLOB, (err, matches) => {
            matches.forEach(path => cacheTemplate(path));

            resolve();
        });
    })
}

function formatHRTime(time) {
    return (time[0] * 1000) + (time[1] / 1000000);
}