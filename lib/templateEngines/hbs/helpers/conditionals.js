module.exports = (Handlebars) => {
    Handlebars.registerHelper('ifCond', function (v1, operator, v2, options) {
        return !!eval(`v1 ${operator} v2`) ? options.fn(this) : options.inverse(this);
    });
}