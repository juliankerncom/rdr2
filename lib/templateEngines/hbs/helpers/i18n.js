const i18nHelpers = require('../../shared/helpers/i18n');

module.exports = (Handlebars, i18nProvider) => {
    Handlebars.registerHelper('mssg', function (messageKey, options) {
        let translated = i18nHelpers.mssg(messageKey, options.data.root.TR.locale, i18nProvider);
        translated = translated.replace(/\$block\./, '');

        if (Object.keys(options.hash).length) {
            const tokens = translated.match(/{([\w]+)}/g) || [];

            tokens.forEach(token => {
                const variable = token.replace(/[{}]/g, '');
                translated = translated.replace(new RegExp(token, 'g'), options.hash[variable] ? options.hash[variable] : '');
                console.log('tokens', variable, options.hash, options.hash[variable]);
            })

            console.log()
        }

        return translated;
    });
}