const dateHelpers = require('../../shared/helpers/date');


module.exports = (Handlebars) => {
    Handlebars.registerHelper('date', function (options) {
        console.log('DATE', options);
        return dateHelpers.date(options.data.root.TR.locale, options.hash.format);
    });
}