const urlHelpers = require('../../shared/helpers/url');

module.exports = (Handlebars) => {
    Handlebars.registerHelper('staticUrl', function (staticPath, options) {
        return urlHelpers.url(staticPath, options.data.root.TR.baseStatic)
    });

    Handlebars.registerHelper('imageUrl', function (urlPath, options) {
        return urlHelpers.url(urlPath, options.data.root.TR.imagesBaseUrl)
    });

    Handlebars.registerHelper('url', function (urlPath, options) {
        return urlHelpers.url(urlPath, options.data.root.TR.baseUrl)
    });
}