module.exports = (Handlebars) => {
    Handlebars.registerHelper('concat', function (...params) {
        params.pop();
        return params.join('');
    });

    Handlebars.registerHelper('lcase', function (str) {
        return (str + '').toLowerCase();
    });
}