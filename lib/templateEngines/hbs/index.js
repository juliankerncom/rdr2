const fs = require('fs');

// var logger = require('../utils/logger');
const Handlebars = require('handlebars');

let compiled = {};
let templatesList = {};

module.exports = class DustEngine {
    constructor(templates, i18nProvider) {
        templatesList = templates;
        this.context = {
            languages: ['de', 'en']
        };

        require('./helpers')(Handlebars, i18nProvider);

        Object.keys(templatesList).forEach(p => cacheTemplate(p));
    }

    templateChanged(key, path) {
        cacheTemplate(key, path);
    }

    async render(template, context, data) {
        // let contextData = dust.context(Object.assign({}, this.context, context)).push(data);
        let contextData = Object.assign({}, { global: this.context }, { TR: context }, data);

        // console.log('contextData', contextData);

        return compiled[template](contextData);
    }
}

function cacheTemplate(key, path) {
    let src = fs.readFileSync(templatesList[key].path) + '';
    src = handleLPSnippets(src);
    
    if (/\.partial$/.test(key)) {
        Handlebars.registerPartial(key.replace('.partial', ''), src);
    } else {
        compiled[key] = Handlebars.compile(src);
    }
}

function handleLPSnippets(text) {
    return text.replace(/\[lp\.([\w\.]*)\]/gi, '{{mssg "$1"}}');
}