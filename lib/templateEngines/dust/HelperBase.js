/**
 * BaseClass for all helpers. wraps the interface to the dust context and offers convenience methods
 *
 * @param dust
 * @param chunk
 * @param context
 * @param bodies
 * @param parameters
 * @param attachments
 * @constructor
 */
function HelperBase(dust, chunk, context, bodies, parameters, attachments) {
    this._dust = dust;
    this._parameters = parameters || {};
    this._chunk = chunk;
    this._context = context;
    this._blocks = bodies;
    this._attachments = attachments || {};

    if(!this._context.global) {
        this._context.global = {};
    }
}

/**
 * acces to the attachments that have been established for dust
 *
 * @param attachmentName
 * @returns {*|null}
 */
HelperBase.prototype.attachment = function(attachmentName) {
    return this._attachments[attachmentName] || null;
};

/**
 * returns all parameterNames from the call context
 *
 * @returns {Array}
 */
HelperBase.prototype.parameterKeys = function() {
    return Object.keys(this._parameters);
};

/**
 * returns the value of a certain parameter
 *
 * @param key
 * @param defaultValue
 * @returns {*}
 */
HelperBase.prototype.parameter = function(key, defaultValue) {
    if (this._parameters[key] !== undefined) {
        return this._dust.helpers.tap(this._parameters[key], this._chunk, this._context);
    } else {
        return defaultValue !== undefined ? defaultValue : null;
    }
};

/**
 * return all parameters
 * @returns object
 */
HelperBase.prototype.parameters = function() {
    var that = this;
    var ret = {};

    Object.keys(this._parameters).forEach(function(parameterKey) {
        ret[parameterKey] = that._dust.helpers.tap(that._parameters[parameterKey], that._chunk, that._context);
    });

    return ret;
};

/**
 * writes a string to the template chunk
 *
 * @param value
 */
HelperBase.prototype.write = function(value) {
    this._chunk.write(value);
};

/**
 * return the data context
 *
 * @returns {*}
 */
HelperBase.prototype.context = function() {
    return this._context;
};

/**
 * returns the size of the current stack if in stack context
 *
 * @returns {int|null}
 */
HelperBase.prototype.stackSize = function() {
    return this._context.stack.of !== undefined ? this._context.stack.of : null;
};

/**
 * return the current stack index if in stack context
 *
 * @returns {int|null}
 */
HelperBase.prototype.stackIndex = function() {
    return this._context.stack.index !== undefined ? this._context.stack.index : null;
};

/**
 * flags the stack context
 *
 * @returns {boolean}
 */
HelperBase.prototype.isStackContext = function() {
    return this.stackSize() !== null && this.stackIndex() !== null
};

/**
 * renders a certain block
 *
 * @param body the block that should be rendered. defaults to the standard block if set
 * @param context
 */
HelperBase.prototype.render = function(body, context) {
    body = body || this._blocks.block;
    context = context || this._context;

    if (body) {
        this._chunk.render(body, context);
    }
};

/**
 * takes the current chunk and manipulates the rendered data via the tap helper so that the chunk will not be altered
 * returns the complete rendered block as a string
 * does not work in a streaming environment
 *
 * @param block
 * @param context
 * @returns {string}
 */
HelperBase.prototype.getRenderedBlock = function(block, context) {
    var ret = '';

    this._chunk.tap(function (data) {
        ret += data;
        return '';
    }).render(block || this._blocks.block, context || this._context).untap();

    return ret;
};

/**
 * returns a certain block
 *
 * @param blockName
 * @returns {*|null}
 */
HelperBase.prototype.block = function(blockName) {
    blockName = blockName || 'block';

    return this._blocks[blockName] || null;
};

/**
 * return the list of all available blocks
 * @returns {*}
 */
HelperBase.prototype.blocks = function() {
    return this._blocks;
};

module.exports = HelperBase;