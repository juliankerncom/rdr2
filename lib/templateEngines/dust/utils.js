/**
 * path helper does a compatible path.join
 *
 * @param base
 * @param path
 * @returns {*}
 */

module.exports.pathBuilder = function pathHelper(base, path) {
    base = base || '/';
    path = path || '';

    if (base[base.length - 1] === '/') {
        base = base.substr(0, base.length - 1);
    }

    if(path) {
        if (path[0] !== '/') {
            path = '/' + path;
        }
    } else {
        path = '';
    }

    return base + path;
};