const fs = require('fs');

// var logger = require('../utils/logger');
var pluginLoader = require('./Loader');

let dust = {};
let templatesList = {};

module.exports = class DustEngine {
    constructor(templates, i18nProvider) {
        templatesList = templates;
        this.context = {
            languages: ['de', 'en']
        };

        try {
            dust = require('dustjs-helpers');
        }
        catch(e) {
            console.log('Dust helpers not found');
            // logger.warn({evt:'dustHelpersNotFound'});

            dust = require('dustjs-linkedin');
            dust.helpers = {};
        }

        dust.silenceErrors = true;

        setWhitespaceCompressor(Renderer.config, dust);

        pluginLoader.enhance(dust, { i18n: i18nProvider });

        Object.keys(templatesList).forEach(p => cacheTemplate(p));
    }

    templateChanged(key, path) {
        cacheTemplate(key, path);
    }

    async render(template, context, data) {
        let contextData = dust.context(Object.assign({}, this.context, context)).push(data);

        return new Promise((resolve, reject) => {
            dust.render(template, contextData, (err, out) => {
                if (err) return reject(err);

                resolve(out);
            })
        })
    }
}

function cacheTemplate(key, path) {
    let src = fs.readFileSync(templatesList[key].path) + '';

    src = handleLPSnippets(src);

    const compiled = dust.compile(src, key);
    dust.loadSource(compiled);
}

function setWhitespaceCompressor(configuration, dust) {
    if (configuration.stripWhitespaces === true) {
        dust.optimizers.format = function(ctx, node) {
            return ['buffer', ' '];
        };
    } else {
        //*not* compressing any whitespaces
        dust.optimizers.format = function(ctx, node) {
            return node;
        };
    }
}

function handleLPSnippets(text) {
    return text.replace(/\[lp\.([\w\.]*)\]/gi, '{@mssg key="$1"/}');
}