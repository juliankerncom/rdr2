const moment = require('moment-timezone');
const util = require('util');

const DELIMITER = '-';
const OUTPUT_RANGE = 1;

function calculateTimeSpanUntil(toDate) {
    if (toDate === '' || !moment(toDate, moment.ISO_8601, true).isValid()) {
        return 0;
    }

    const diff = Math.floor(Math.abs(moment.duration(moment().diff(moment(toDate))).asWeeks()));

    if (diff === 0) {
        return 0;
    }
    return util.format('%s%s%s', diff, DELIMITER, (diff + OUTPUT_RANGE));
}

/**
 * retrieve timeSpan until given date in weeks
 *
 * @returns {*}
 */
module.exports = function () {
    this.write(calculateTimeSpanUntil(this.parameter('date', '')));
};