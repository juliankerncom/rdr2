var utils = require('../utils');

var KEY_PATH = 'path';
var AUDIOURL_CHECKSUM_REGEX = /\/.*\/(\d).*?_(.(.).*?)\-.(.).*/;

function calculateAudioUrlChecksum(path) {
    var match = AUDIOURL_CHECKSUM_REGEX.exec(path);

    return match[1] + 'x4' + match[3] + '6' + match[4] + 'p.' + match[2];
}

function audioUrl() {
    var path = this.parameter(KEY_PATH);

    var url = this.context().global.protocol + '://' + utils.pathBuilder(this.context().global.audioHost, path);

    if (!this.parameter('noChecksum')) {
        url += '?p=' + encodeURIComponent(calculateAudioUrlChecksum(path));
    }

    this.write(url);
}

function videoUrl() {
    this.write(this.context().global.protocol + '://' + utils.pathBuilder(this.context().global.videoHost, this.parameter(KEY_PATH)));
}

function thumbnailUrl() {
    //really simple file extension replacement (maybe needs a little bit more sophisticated handling, but for now it works...)
    var url = utils.pathBuilder(this.context().global.thumbnailsBaseUrl, this.parameter(KEY_PATH));

    if (this.parameter('type')) {
        url = url.substr(0, url.lastIndexOf(".")) + "." + this.parameter('type');
    }

    this.write(url);
}

function videoThumbnailUrl() {
    this.write(utils.pathBuilder(this.context().global.videoThumbnailsBaseUrl, this.parameter(KEY_PATH)));
}

function staticUrl() {
    this.write(utils.pathBuilder(this.context().global.baseStatic, this.parameter(KEY_PATH)));
}

function imageUrl() {
    this.write(utils.pathBuilder(this.context().global.imagesBaseUrl, this.parameter(KEY_PATH)));
}

function url() {
    this.write(utils.pathBuilder(this.context().global.baseUrl, this.parameter(KEY_PATH)));
}

var BDBO_HOST = 'bdbo.thomann.de';
var BDBO_BASE_PATH = 'thumb';
var BDBO_PATH_INFIX = ['pics', 'bdbo'];
var BDBO_DEFAULT_RESOLUTION = 'bdb1200';

function bdboUrl() {
    var pathElements = [BDBO_BASE_PATH, this.parameter('resolution', BDBO_DEFAULT_RESOLUTION)]
        .concat(BDBO_PATH_INFIX)
        .concat(this.parameter('fileName'));

    this.write(this.context().global.protocol + '://' + BDBO_HOST + '/' + pathElements.join('/'));
}

module.exports = {
    url: url,
    staticUrl: staticUrl,
    imageUrl: imageUrl,
    audioUrl: audioUrl,
    videoUrl: videoUrl,
    thumbnailUrl: thumbnailUrl,
    videoThumbnailUrl: videoThumbnailUrl,
    bdboUrl: bdboUrl
};