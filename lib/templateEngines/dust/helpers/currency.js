var acc = require('accounting');

var TAG_POST = 'TAG_POST';
var TAG_PRE = 'TAG_PRE';

var SEP_COMMA = ',';
var SEP_DOT = '.';
var SEP_SPACE = ' ';
var SEP_NONE = '';

var COUNTRY_DEFAULT = 'de';
var CURRENCY_DEFAULT = 'EUR';

var currSymbols = {
    EUR: '€',
    GBP: '£',
    USD: '$'
};

var KEY_PRIMARY_CURRENCY = 'primary';
var KEY_SECONDARY_CURRENCY = 'secondary';

var currencyConfiguration = {
    de: { TAG_POS: TAG_POST, SYMBOL_SEP: SEP_SPACE, THSD_SEP: SEP_DOT, DEC_SEP: SEP_COMMA },
    at: { TAG_POS: TAG_PRE, SYMBOL_SEP: SEP_SPACE, THSD_SEP: SEP_DOT, DEC_SEP: SEP_COMMA },
    gb: { TAG_POS: TAG_PRE, SYMBOL_SEP: SEP_NONE, THSD_SEP: SEP_COMMA, DEC_SEP: SEP_DOT },
    us: { TAG_POS: TAG_PRE, SYMBOL_SEP: SEP_NONE, THSD_SEP: SEP_COMMA, DEC_SEP: SEP_DOT },
    ie: { TAG_POS: TAG_PRE, SYMBOL_SEP: SEP_NONE, THSD_SEP: SEP_COMMA, DEC_SEP: SEP_DOT },
    be: { TAG_POS: TAG_POST, SYMBOL_SEP: SEP_SPACE, THSD_SEP: SEP_DOT, DEC_SEP: SEP_COMMA },
    gr: { TAG_POS: TAG_POST, SYMBOL_SEP: SEP_SPACE, THSD_SEP: SEP_DOT, DEC_SEP: SEP_COMMA },
    fr: { TAG_POS: TAG_POST, SYMBOL_SEP: SEP_SPACE, THSD_SEP: SEP_DOT, DEC_SEP: SEP_COMMA },
    lu: { TAG_POS: TAG_POST, SYMBOL_SEP: SEP_SPACE, THSD_SEP: SEP_SPACE, DEC_SEP: SEP_COMMA },
    es: { TAG_POS: TAG_POST, SYMBOL_SEP: SEP_SPACE, THSD_SEP: SEP_DOT, DEC_SEP: SEP_COMMA },
    it: { TAG_POS: TAG_PRE, SYMBOL_SEP: SEP_SPACE, THSD_SEP: SEP_DOT, DEC_SEP: SEP_COMMA },
    dk: { TAG_POS: TAG_POST, SYMBOL_SEP: SEP_SPACE, THSD_SEP: SEP_DOT, DEC_SEP: SEP_COMMA },
    se: { TAG_POS: TAG_POST, SYMBOL_SEP: SEP_SPACE, THSD_SEP: SEP_SPACE, DEC_SEP: SEP_COMMA },
    fi: { TAG_POS: TAG_POST, SYMBOL_SEP: SEP_SPACE, THSD_SEP: SEP_SPACE, DEC_SEP: SEP_COMMA },
    nl: { TAG_POS: TAG_PRE, SYMBOL_SEP: SEP_SPACE, THSD_SEP: SEP_DOT, DEC_SEP: SEP_COMMA },
    pt: { TAG_POS: TAG_PRE, SYMBOL_SEP: SEP_SPACE, THSD_SEP: SEP_DOT, DEC_SEP: SEP_COMMA },
    pl: { TAG_POS: TAG_POST, SYMBOL_SEP: SEP_SPACE, THSD_SEP: SEP_SPACE, DEC_SEP: SEP_COMMA },
    cz: { TAG_POS: TAG_POST, SYMBOL_SEP: SEP_SPACE, THSD_SEP: SEP_SPACE, DEC_SEP: SEP_COMMA },
    ro: { TAG_POS: TAG_POST, SYMBOL_SEP: SEP_SPACE, THSD_SEP: SEP_DOT, DEC_SEP: SEP_COMMA }
};

function currencyFormat() {
    var contextCountry = (this.context().global.country || COUNTRY_DEFAULT).toLowerCase();
    var country = this.parameter('country', false);
    if (country) {
        country = country.toLowerCase();
    }
    var value = this.parameter('value', 0);

    var contextCurrencyPrimary = this.context().global.currencyPrimary || CURRENCY_DEFAULT;
    var contextCurrencySecondary = this.context().global.currencySecondary || CURRENCY_DEFAULT;
    var currency = this.parameter('currency', false);
    var myCurrency;

    var resultSuppressDecimals = this.parameter('suppDec', true);
    var suppressDecimals = !(resultSuppressDecimals == 'false' || !resultSuppressDecimals);

    var resultSuppressSpace = this.parameter('suppSpace', false);
    var suppressSpace = (resultSuppressSpace === 'true' || resultSuppressSpace === true);

    if (currency) {
        if (currency === KEY_PRIMARY_CURRENCY) {
            myCurrency = contextCurrencyPrimary;
        } else if (currency === KEY_SECONDARY_CURRENCY) {
            myCurrency = contextCurrencySecondary;
        } else {
            myCurrency = currency;
        }
    } else {
        myCurrency = contextCurrencyPrimary;
    }
    myCurrency = myCurrency.toUpperCase();

    var formatConfig = currencyConfiguration[country]
        || currencyConfiguration[contextCountry]
        || currencyConfiguration[COUNTRY_DEFAULT];

    var useConfig = {
        precision: 2,
        decimal: formatConfig.DEC_SEP,
        thousand: formatConfig.THSD_SEP
    };

    if (suppressDecimals && +value % 1 == 0) {
        useConfig.precision = 0;
    }

    var formatted = acc.formatNumber(value, useConfig);

    var result = '';
    var currencySymbol = currSymbols[myCurrency] || myCurrency;

    var space = formatConfig.SYMBOL_SEP;
    if (suppressSpace) {
        space = '';
    }

    if (formatConfig.TAG_POS === TAG_PRE) {
        result = currencySymbol + space + formatted;
    } else {
        result = formatted + space + currencySymbol;
    }

    this.write(result);
}

function currencySymbol() {
    var currency = this.parameter('currency');
    this.write(currSymbols[currency] || currency);
}

module.exports = {
    currency: currencyFormat,
    currencySymbol: currencySymbol
};
