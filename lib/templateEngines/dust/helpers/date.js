var moment = require('moment-timezone');

var HAS_TIMEZONE = /((\+|-)\d{2}:?\d{2}|Z)/;
var IS_UTC = /(Z|(\+|-)00:?00)/;

// data structure that maps keys to a specific date format
var dateFormats = {
    de: {
        date: 'DD.MM.YYYY',
        monthYear: 'MM.YYYY',
        dayMonth: 'DD.MM.',
        dateTime: 'DD.MM.YYYY - HH:mm',
        time: 'HH:mm'
    },
    en: {
        date: 'YYYY/MM/DD',
        monthYear: 'YY/MM',
        dayMonth: 'MM/DD/',
        dateTime: 'HH:mm - YYYY/MM/DD',
        time: 'hh:mm'
    }
};

function parseDate(subject) {
    if (subject === 'now' || !subject) {
        return moment();
    }

    if (IS_UTC.test(subject)) {
        return moment.utc(subject)
    }

    return moment(subject, 'YYYY-MM-DD HH:mm Z');
}

function normalizeDate(subject, myDate) {
    var timeZoneOffset;

    //we're expecting UTC, thus we're subtracting the local timezone if timezone is not set already
    if (hasNoTimeZoneOrIsUtc(subject)) {
        timeZoneOffset = moment.tz.zone('Europe/Berlin').parse(myDate);

        myDate.subtract(timeZoneOffset, 'minutes');
    }
}
/**
 * helper that is used to do dateformats:
 * {@date subject=myDate style=date /} (renders e.g. de: 1. Jan 2017 or en: Jan 1 2017)
 */
module.exports = function () {
    var lang = this.context().global.language || 'de';
    var formats = dateFormats[lang] || dateFormats.de;
    var subject = this.parameter('subject', '');
    var style = this.parameter('style', '');
    var myFormat = formats[style] || style;
    var myDate = parseDate(subject);

    normalizeDate(subject, myDate);
    myDate.locale(lang);

    if (!myFormat || !subject || !myDate || !myDate.isValid()) {
        if (this.context().global.developmentMode) {
            this.write('DATE_ERROR');
        }
    } else {
        this.write(myDate.format(myFormat));
    }
};

function hasNoTimeZoneOrIsUtc(subject) {
    return !HAS_TIMEZONE.test(subject) || IS_UTC.test(subject)
}