/**
 * marks the first incarnation, accepts an else block
 */
function first() {
    if (this.isStackContext()) {
        if (this.stackIndex() == 0) {
            this.render();
        } else if (this.block('else')) {
            this.render(this.block('else'));
        }
    }
}

/**
 * marks the last incarnation, accepts an else block
 */
function last() {
    if (this.isStackContext()) {
        if (this.stackIndex() == this.stackSize() - 1) {
            this.render();
        } else if (this.block('else')) {
            this.render(this.block('else'));
        }
    }
}

/**
 * returns the natural number one would use when counting (idx is count-1)
 */
function nr() {
    var offset = this.parameter('offset') ? parseInt(this.parameter('offset'), 10) : 0;

    if (this.isStackContext()) {
        this.write(this.stackIndex() + 1 + offset);
    }
}

/**
 * renders the attached block on a predefined position in the loop
 */
function listOffset() {
    var offset = this.parameter('offset');

    if (!isNaN(offset)) {
        offset = [offset];
    } else {
        offset = (offset + '').split(',');
    }

    offset = offset.map(function(value) {
        return +value;
    });

    var idx = this.stackIndex();
    var num = this.stackSize();

    if (idx !== null && num !== null) {
        var offsets = offset.map(function(value) {
            if (value < 0) {
                value = num + value;
            }

            return value;
        });

        if (offsets.indexOf(idx) >= 0 && this.block()) {
            this.render();
        } else if (this.block('else')) {
            this.render(this.block('else'));
        }
    }
}

module.exports = {
    first: first,
    last: last,
    nr: nr,
    listOffset: listOffset
};