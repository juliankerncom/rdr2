/**
 * marks alternating (odd an even incarnations of loop entries
 * attention: odd and even relates to the loop INDEX, not the loop iteration number
 */
function decider (remainder, elseBlock) {
    if (this.isStackContext()) {
        var elseVariation = this.block('else') || this.block(elseBlock) || null;

        if (this.stackIndex() % 2 === remainder) {
            this.render();
        } else if (elseVariation) {
            this.render(elseVariation);
        }
    }
}

module.exports = {
    even: function () {
        decider.call(this, 0, 'odd');
    },
    odd: function () {
        decider.call(this, 1, 'even');
    }
};