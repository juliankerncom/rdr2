/**
 * helpers that outputs the block's context if development is set
 * usage: {@debug}this is debug content{/debug}
 */
function debug() {
    if (this.context().global.developmentMode) {
        this.render();
    }
}

/**
 * helper that outputs its main block content if in developmentMode and the else block if not
 * usage: {@devMode}this is debug content{/devMode}
 */
function devMode() {
    if (this.context().global.developmentMode) {
        this.render();
    } else if (this.block('else')) {
        this.render(this.block('else'));
    }
}

/**
 * helper that outputs its main block content if in devEnvironment and the else block if not
 * usage: {@devEnvironment}this is dev environment content{/devEnvironment}
 */
function devEnvironment() {
    if (this.context().global.devEnvironment) {
        this.render();
    } else if (this.block('else')) {
        this.render(this.block('else'));
    }
}

/**
 * Helper that outputs its main block content if the feature switch given as 'feature' is enabled.
 * Otherwise the given {:else} block is output when present.
 * usage: {@isFeatureEnabled feature="foo"}enabled{:else}disabled}{/isFeatureEnabled}
 */
function isFeatureEnabled() {
    var feature = this.parameter('feature');
    var enabledFeatureSwitches = this.context().global.enabledFeatureSwitches;

    if (!feature || !enabledFeatureSwitches) {
        return;
    }

    if (enabledFeatureSwitches.indexOf(feature) !== -1) {
        this.render();
    } else if (this.block('else')) {
        this.render(this.block('else'));
    }
}

/**
 * helper that outputs its main block content if in productionMode and the else block if not
 * usage: {@prodMode}this is live content{/prodMode}
 */
function prodMode() {
    if (!this.context().global.developmentMode) {
        this.render();
    } else if (this.block('else')) {
        this.render(this.block('else'));
    }
}

/**
 * helper that outputs its main block content if not devEnvironment and the else block if not
 * usage: {@prodEnvironment}this is prod environment content{/prodEnvironment}
 */
function prodEnvironment() {
    if (!this.context().global.devEnvironment) {
        this.render();
    } else if (this.block('else')) {
        this.render(this.block('else'));
    }
}

module.exports = {
    debug: debug,
    devMode: devMode,
    isFeatureEnabled: isFeatureEnabled,
    prodMode: prodMode,
    devEnvironment: devEnvironment,
    prodEnvironment: prodEnvironment
};