
function provide () {
    var that = this;
    var localContext = this.context().push(this.parameters());

    var blockData = {};

    Object.keys(this.blocks()).forEach(function(blockKey) {
        if (blockKey == 'block') return;

        blockData[blockKey] = that.getRenderedBlock(that.block(blockKey), localContext);
    });

    this.render(this.block(), localContext.push(blockData));
}

module.exports = provide;