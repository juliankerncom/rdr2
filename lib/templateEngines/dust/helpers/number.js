var acc = require('accounting');
var hoek = require('hoek');

var COUNTRY_DEFAULT = 'de';

/**
 * data structure that maps keys to a specific number format
 */
var numberFormats = {
    de: {
        precision: 2,
        decimal: ',',
        thousand: '.'
    },
    gb: {
        precision: 2,
        decimal: '.',
        thousand: ','
    }
};

/**
 * data structure that maps the format of specific languages to another language
 */
var dateAssignments = {
    fr: numberFormats.de, es: numberFormats.de, se: numberFormats.de, ie: numberFormats.gb, fi: numberFormats.de, dk: numberFormats.de,
    nl: numberFormats.de, be: numberFormats.de, pt: numberFormats.de, gr: numberFormats.de, pl: numberFormats.de, cz: numberFormats.de,
    ro: numberFormats.de
};

function getNumberFormatByLanguage(lang, altLang) {
    return     numberFormats[lang]
            || dateAssignments[lang]
            || numberFormats[altLang]
            || dateAssignments[altLang]
            || numberFormats.de;
}

function numberFormat() {
    var contextCountry = (this.context().global.country || COUNTRY_DEFAULT).toLowerCase();
    var country = this.parameter('country', false);
    if (country) {
        country = country.toLowerCase();
    }

    var value = this.parameter('value', 0);
    var precision = this.parameter('precision', false);
    var thousand = this.parameter('thousand', false);
    var decimal = this.parameter('decimal', false);

    var numberFormat = hoek.clone(getNumberFormatByLanguage(country, contextCountry));

    numberFormat.precision = precision !== false ? precision : numberFormat.precision;
    numberFormat.thousand = thousand !== false ? thousand : numberFormat.thousand;
    numberFormat.decimal = decimal !== false ? decimal : numberFormat.decimal;

    this.write(acc.formatNumber(value, numberFormat));
}

module.exports = numberFormat;