var PLAIN_TEXT_PATTERN = /^!(.*)!$/;
var NO_PLACEHOLDER_PATTERN = /\{[^\}]*\}/g;

var SNIPPET_NOT_FOUND = 'SNIPPET_NOT_FOUND';
var KEY_SEPERATOR = '.';
var KEY_IDENTIFIER = 'key';
var KEY_ALTERNATIVEKEY_IDENTIFIER = 'alternativeKey';
var KEY_FALLBACKTEXT_IDENTIFIER = 'fallbackText';
var KEY_CUSTOMIZE_MACROS = 'customizeMacros';
var KEY_SUBJECT = 'subject';
var KEY_KEYPREFIX = 'keyPrefix';
var KEY_BULKPARAMTERS = 'bulkParameters';

var KEY_PLURAL_ANY = 'any';

var FALLBACK_LANGUAGE = 'en';

function escapeForRegex(s) {
    return s.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&");
}

function isProduction() {
    return process.env.NODE_ENV !== 'development'
        && process.env.NODE_ENV !== 'develop'
        && process.env.NODE_ENV !== 'staging';
}

function getLanguage(i18n, key, language, country, parameters) {
    var keyParts = key.split(KEY_SEPERATOR);
    var packName = keyParts.shift();
    var snippetKey = keyParts.join(KEY_SEPERATOR);

    var result = i18n.getSnippet(packName, snippetKey, language, country);
    if (result instanceof Error) {
        return null;
    }

    Object.keys(parameters).forEach(function (parameterKey) {
        var parameterValue = parameters[parameterKey];

        if (parameterValue !== null && parameterValue !== false) {
            parameterValue = parameterValue + ''; //parameters are userland and are always evil.

            var potentialValue = getLanguage(i18n, parameterValue, language, country, {});
            parameterValue = potentialValue || parameterValue;

            result = result.replace(new RegExp('\\\{' + escapeForRegex(parameterKey) + '\\\}', 'gi'), parameterValue);
        }
    });

    return result.replace(NO_PLACEHOLDER_PATTERN, '');
}

function collectParameters(helperCallContext, exclusionList) {
    var parameters = {};
    helperCallContext.parameterKeys().forEach(function (key) {
        if (exclusionList.indexOf(key) >= 0) return;

        parameters[key] = helperCallContext.parameter(key);
    });

    //implements the 'block as parameters' functionality of the mssg helper
    //@see: https://collaboratr.thomann.de/display/COMP/Helpers#Helpers-mssg
    Object.keys(helperCallContext.blocks()).forEach(function (blockKey) {
        parameters['$block.' + blockKey] = helperCallContext.getRenderedBlock(helperCallContext.block(blockKey));

        if (!parameters[blockKey]) {
            parameters[blockKey] = helperCallContext.getRenderedBlock(helperCallContext.block(blockKey));
        }
    });

    //implements the 'bulkparameters' functionality of the mssg helper
    //@see: https://collaboratr.thomann.de/display/COMP/Helpers#Helpers-mssg
    var bulkParameters = parameters[KEY_BULKPARAMTERS] || null;
    if (bulkParameters !== null) {
        Object
            .keys(bulkParameters)
            .forEach(function(subParameterKey) {
                if (!parameters[subParameterKey]) {
                    parameters[subParameterKey] = bulkParameters[subParameterKey];
                }
            });

        delete parameters[KEY_BULKPARAMTERS];
    }

    return parameters;
}

function tryAllLanguages(i18n, keyOrKeys, languages, country, parameters) {
    if (languages.indexOf(FALLBACK_LANGUAGE) === -1) {
        languages.push(FALLBACK_LANGUAGE);
    }

    var keys = !Array.isArray(keyOrKeys) ? [keyOrKeys] : keyOrKeys;

    for (var languageIndex = 0; languageIndex < languages.length; languageIndex++) {
        for (var keyIndex = 0; keyIndex < keys.length; ++keyIndex) {
            var languageValue = getLanguage(i18n, keys[keyIndex], languages[languageIndex], country, parameters);
            if (languageValue !== null) {
                return languageValue;
            }
        }
    }

    return null;
}

function emptyResultHandling(key, result) {
    //writing a snippet not found message in development
    if (!result) {
        if (!isProduction()) {
            result = SNIPPET_NOT_FOUND + ' (' + key + ')';
        } else {
            result = '';
        }
    }

    return result;
}

/**
 * helper that realizes the dynamic output of language resources
 */
function mssg() {
    var myI18n = this.context().global.i18nOverwrite ? this.context().global.i18nOverwrite : this.attachment('i18n');
    var languages = this.context().global.languages;
    // console.log('CONTEXT', this.context());
    var country = this.context().global.country;
    var key = this.parameter(KEY_IDENTIFIER, '') + '';
    var alternativeKey = this.parameter(KEY_ALTERNATIVEKEY_IDENTIFIER, null);
    var fallbackText = this.parameter(KEY_FALLBACKTEXT_IDENTIFIER, null);
    var customizeMacros = this.parameter(KEY_CUSTOMIZE_MACROS, null);

    //there is the possibility to pass plain text as a key to the helper.
    //the helper will output this plain text directly, plain text is marked with exclamation marks (!this is plain text!)
    var matches = key.match(PLAIN_TEXT_PATTERN);
    if (matches) {
        return this.write(matches[1]);
    }

    var parameters = collectParameters(this, [KEY_IDENTIFIER, KEY_ALTERNATIVEKEY_IDENTIFIER, KEY_FALLBACKTEXT_IDENTIFIER, KEY_CUSTOMIZE_MACROS]);
    var keys = [key];

    //the customizeMacros logic implies that there maybe a snippet that has a country specific value (the old NC way)
    //example: the key 'foo' yields 'color' in english, the key 'foo.gb' yields 'colour' as this is the GB specific spelling
    //the parameter customizeMacros causes the helper to also look for the variation with the country key attached to the original key
    //as it has higher precedence we use keys.unshift here
    if (customizeMacros === 'true') {
        keys.unshift(key + KEY_SEPERATOR + country);
    }

    if (alternativeKey) {
        keys.push(alternativeKey);
    }

    var result = tryAllLanguages(myI18n, keys, languages, country, parameters);
    if (!result && fallbackText) {
        result = fallbackText;
    }

    return this.write(emptyResultHandling(key, result));
}

function plural() {
    var result;
    var myI18n = this.context().global.i18nOverwrite ? this.context().global.i18nOverwrite : this.attachment('i18n');
    var languages = this.context().global.languages;
    var country = this.context().global.country;
    var subject = parseInt(this.parameter(KEY_SUBJECT, ''), 10);
    var keyPrefix = this.parameter(KEY_KEYPREFIX, '');
    var parameters = collectParameters(this, [KEY_KEYPREFIX]);

    if (isNaN(subject)) return;

    var key = keyPrefix + KEY_SEPERATOR + subject;

    result = tryAllLanguages(myI18n, key, languages, country, parameters);
    if (!result) {
        key = keyPrefix + KEY_SEPERATOR + KEY_PLURAL_ANY;
        result = tryAllLanguages(myI18n, key, languages, country, parameters);
    }

    this.write(emptyResultHandling(key, result));
}

function mssgExists() {
    var elseVariation = this.block('else') || null;
    var myI18n = this.context().global.i18nOverwrite ? this.context().global.i18nOverwrite : this.attachment('i18n');
    var languages = this.context().global.languages;
    var country = this.context().global.country;
    var subject = this.parameter(KEY_SUBJECT, '') + '';

    if (!subject) return;

    if (tryAllLanguages(myI18n, subject, languages, country, {})) {
        this.render();
    } else if (elseVariation) {
        this.render(elseVariation);
    }
}

module.exports = {
    i18n: mssg,
    mssg: mssg,
    plural: plural,
    mssgExists: mssgExists
};
