'use strict'

// const Hypher = require('hypher')

// const patterns = {
//     de: require('hyphenation.de'),
//     en: require('hyphenation.en-gb'),
//     fr: require('hyphenation.fr'),
//     es: require('hyphenation.es'),
//     fi: require('hyphenation.fi'),
//     pt: require('hyphenation.pt'),
//     cs: require('hyphenation.cs'),
//     sv: require('hyphenation.sv'),
//     da: require('hyphenation.da'),
//     nl: require('hyphenation.nl'),
//     pl: require('hyphenation.pl'),
//     it: require('hyphenation.it')
// }

// const hyphenations = Object.keys(patterns).reduce((carry, patternKey) => {
//     let pattern = patterns[patternKey]
//     pattern.leftmin = 3
//     pattern.rightmin = 4

//     carry[patternKey] = new Hypher(pattern)
//     return carry
// }, {})

const WORD_SPLIT_PATTERN = /\s+/g

function splitHyphenate(text, threshold, hypher) {
    return text.split(WORD_SPLIT_PATTERN)
        .map(word => {
            if (word.length >= threshold) {
                return hypher.hyphenateText(word)
            }

            return word
        })
        .join(' ')
}

function hyphenate() {
    let threshold = this.parameter('threshold', false)
    let blockContent = this.getRenderedBlock(this.block())
    let lang = this.context().global.language

    // let hypher = lang && hyphenations[lang] ? hyphenations[lang] : null
    // if (hypher) {
    //     if (threshold) {
    //         blockContent = splitHyphenate(blockContent, threshold, hypher)
    //     } else {
    //         blockContent = splitHyphenate(blockContent, 4, hypher)
    //     }
    // }

    this.write(blockContent)
}

module.exports = {
    hypher: hyphenate,
    hypherhypher: hyphenate
}