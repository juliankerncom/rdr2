/**
 * returns a JSON representation of all parameters
 */
module.exports = function() {
    var subject = this.parameter('subject');

    if (!subject) {
        this.write(JSON.stringify(this.parameters()));
    } else {
        this.write(JSON.stringify(subject));
    }
};