'use strict';

exports.removeWhitespace = function () {
    var content = this.getRenderedBlock()
        .replace(/\s+</mg, '<')
        .replace(/>\s+/mg, '>');

    this.write(content);
};
