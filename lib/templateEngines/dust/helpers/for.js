// var logger = require('../../../utils/logger').logger('helpers.for');

/**
 * countable loop
 */
function forLoop() {
    var from = parseInt(this.parameter('from'), 10) >= 0 ? parseInt(this.parameter('from'), 10) : null;
    var to = parseInt(this.parameter('to'), 10) >= 0 ? parseInt(this.parameter('to'), 10) : null;
    var step = parseInt(this.parameter('step'), 10) > 0 ? parseInt(this.parameter('step'), 10) : 1;

    if (from === null || to === null) return;
    if (to < from) return;
    if (to - from > 1000) {
        // logger.error({evt: 'longLoop', from: from, to: to});

        return;
    }

    for (var i = from; i <= to; i = i + step) {
        var newContext = this.context().push({}, i - from, (to - from) + 1);

        this.render(this.block(), newContext);
    }
}

module.exports = forLoop;