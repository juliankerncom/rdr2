var entities = require('entities');

/**
 * helper that manages the length of strings without any respect to word boundaries
 */
function charlimit() {
    var plainInput = this.parameter('subject', '');
    var numberChars = this.parameter('chars', false);
    var parameterElipsis = this.parameter('elipsis');

    var useElipsis = !(parameterElipsis == 'false' || !parameterElipsis);
    var resultElipsis = '';

    var decodedInput = entities.decodeHTML(plainInput);
    var trimmedDecodedInput = decodedInput;

    if (numberChars !== false) {
        trimmedDecodedInput = decodedInput.substr(0, numberChars).trim();
    }

    if (useElipsis && trimmedDecodedInput.length < decodedInput.length) {
        resultElipsis = '...';
    }

    //has an conversion actually been done?
    if (plainInput.length !== decodedInput.length) {
        trimmedDecodedInput = entities.encodeHTML(trimmedDecodedInput);
    }

    this.write(trimmedDecodedInput + resultElipsis);
}

/**
 * helper that manages the length of strings, respecting word boundaries
 */
function wordlimit() {
    var use = [];

    var plainInput = this.parameter('subject', '');
    var numberChars = this.parameter('chars', false);
    var numberWords = this.parameter('words', false);
    var parameterElipsis = this.parameter('elipsis');

    var useElipsis = !(parameterElipsis == 'false' || !parameterElipsis);
    var resultElipsis = '';

    if (!numberChars && !numberWords) {
        return this.write(plainInput);
    }

    var decodedInput = entities.decodeHTML(plainInput);
    var parts = decodedInput.split(' ');

    var counter = 0;
    parts.every(function(part) {
        if (numberChars) {
            if ((counter + part.length + 1) < numberChars) {
                use.push(part);
                counter += (part.length + 1);
            } else {
                return false;
            }
        } else if (numberWords) {
            if (counter < numberWords) {
                use.push(part);
                counter++;
            } else {
                return false;
            }
        }

        return true;
    });

    var result = use.join(' ');

    if (useElipsis && result.length < decodedInput.length) {
        resultElipsis = '...';
    }

    if (plainInput.length !== decodedInput.length) {
        result = entities.encodeHTML(result);
    }

    this.write(result + resultElipsis);
}

module.exports = {
    charlimit: charlimit,
    wordlimit: wordlimit
};