var path = require('path');
var fs = require('fs');

var HelperBase = require('./HelperBase');

/**
 * wrapper for each registered helper, causes the invocation of a helper in the context of the HelperBase
 *
 * @param dust
 * @param attachments
 * @param helper
 * @returns {Function}
 */
function wrapHelper(dust, attachments, helper) {
    return function(chunk, context, bodies, parameters) {
        helper.call(new HelperBase(dust, chunk, context, bodies, parameters, attachments));

        return chunk;
    }
}

/**
 * loads all javascript file from a certain directory, requires it and returns the list of required modules
 *
 * @param directory
 */
function requireDir(directory) {
    let filesList = {};
    let files = fs.readdirSync(`${__dirname}/${directory}`);

    files.forEach(path => {
        const filename = path.split('.');
        filename.pop();
        filesList[filename] = require(`./${directory}/${path}`);
    })

    return filesList;
}

/**
 * register a helper to dust
 *
 * @param helperName
 * @param helper
 * @param dust
 * @param attachments
 */
function registerHelper(helperName, helper, dust, attachments) {
    dust.helpers[helperName] = wrapHelper(dust, attachments, helper);
}

/**
 * loads helpers from their directory, wraps and registers them to dust
 *
 * @param dust
 * @param attachments
 */
function enhanceWithHelpers(dust, attachments) {
    var helpers = requireDir('helpers');

    Object.keys(helpers).forEach(function(helperName) {
        var helper = helpers[helperName];

        if(typeof helper === 'function') {
            registerHelper(helperName, helper, dust, attachments);
        } else if(typeof helper === 'object') {
            Object.keys(helper).forEach(function(helperName) {
                registerHelper(helperName, helper[helperName], dust, attachments);
            });
        }
    });
}

/**
 * loads filters from their directory and reigsters them to dust
 *
 * @param dust
 */
function enhanceWithFilters(dust) {
    var filters = requireDir('filters');

    Object.keys(filters).forEach(function(filterName) {
        dust.filters[filterName] = filters[filterName];
    });
}

/**
 * enhance dust with custom helpers and filters
 *
 * @param dust
 * @param attachments
 */
function enhance(dust, attachments) {
    enhanceWithHelpers(dust, attachments);
    enhanceWithFilters(dust);
}

module.exports.enhance = enhance;