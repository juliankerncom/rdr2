/**
 * uppercase a string
 *
 * @param value
 * @returns {*}
 */
module.exports = function(value) {
    return (value + '').toUpperCase();
};