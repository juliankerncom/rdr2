/**
 * Uppercase the first letter
 *
 * @param value
 * @returns {*}
 */
module.exports = function(value) {
    value = value + '';
    return value.substr(0, 1).toUpperCase() + value.substr(1).toLowerCase();
};