var os = require('os');

module.exports = {
    // rdr2 settings

    preloadI18n: true,

    minifyHTML: true,

    i18nWhitelist: '/mobile\\.',

    watchTemplates: true,
    watchI18n: true,


    // =============================================================================================================
    // GENERAL SETTINGS
    // =============================================================================================================

    //the port all the render workers listen to
    port: 8010,

    //port of the server that is used for administration
    portAdministration: 8011,

    // =============================================================================================================
    // CLUSTER SETTINGS
    // =============================================================================================================

    //number of workers that should be spawned in the cluster environment
    numberWorkers: os.cpus().length,

    //indicates if the process should be clustered
    useCluster: true,

    // =============================================================================================================
    // RENDERING
    // =============================================================================================================

    //a list of directories where templates are located, previously templateDir
    templateDirs: ['templates/'],

    //directory where language files are located
    languageDir: 'messages/',

    //indicates of templates should be cached at all
    templateCache: true,

    //indicates if all templates should be compiled with all languages
    prewarmCache: true,

    //indicates if whitespaces should be stripped from templates
    stripWhitespaces: true,

    // indicates if currently on a dev environment
    devEnvironment: false,

    // =============================================================================================================
    // LOGGING
    // =============================================================================================================

    //ATTENTION: under windows the syslog transport collides with the cluster module
    //if you want to use the syslog transport set 'useCluster' to false, should never be done in production
    logging: {
        // The main log level. Valid values are 'debug', 'info', 'warn' or 'error'
        logLevel: 0,

        //which transport should be used to log
        type: 'Console',

        //options that are fed to the logging transport
        options: {
            color: true
        },

        //aggregates rendered templates and logs them in the specified interval
        templateAggregation: {
            active: true,
            interval: 60000
        },

        // Enable periodically tooBusy log events to measure event delay
        enableBusyLogger: true,

        // Defines filters to be applied to log messages before they are sent to the configured transport
        filters: {
            // Pass filters promote all messages matched by any filter to the transport no matter if they are also
            // matched by any reject filter.
            // Filters can match any combination of log message "fields". The following fields are supported
            // - mod => literal match on the log message module
            // - evt => literal match on the log message event
            // - mssg => regular expression match on the log message text
            // - hst => literal match on the host name

            pass: [
//                {
//                    mod: "httpRequest",
//                    evt: "httpRequestFinished"
//                }
            ],

            // Reject filters deny all log messages matched by any filter from being logged, _except_ they are also
            // matched by a pass filter, which has higher precendence.
            reject: [
//                "all"
            ]
        }
    }
};