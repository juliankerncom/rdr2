require('../lib/global');

// console.log('Renderer.config', Renderer.config);

process.on('unhandledRejection', (reason, p) => {
    console.log('Unhandled Rejection at: Promise', p, 'reason:', reason);
    // application specific logging, throwing an error, or other logic here
});

const adapters = require('../lib/adapters');
const dataFormats = require('../lib/dataFormats');
const templateEngines = require('../lib/templateEngines');